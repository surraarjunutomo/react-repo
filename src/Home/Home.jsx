import React, { Component } from 'react';
// import Product from '../Container/Product/Product';
// import BlogPost from '../Container/BlogPost/BlogPost';
import LifeCycle from '../Container/LifeCycle/LifeCycle';

class Home extends Component {
    state = {
        showComponent: true
    }

    // componentDidMount(){
    //     setTimeout(() => {
    //         this.setState({
    //             showComponent : false
    //         })
    //     }, 5000)
    // }
    render() {
        return (
            <div>
                <p>LifeCycle Component</p>
                <hr/>
                <LifeCycle />
                {/* {
                    this.state.showComponent
                    ?
                    <LifeCycle />: null
                } */}
                
            </div>

        )
    }
}

export default Home;
