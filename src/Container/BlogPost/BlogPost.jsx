import React, { Component, Fragment } from "react";
import './BlogPost.css';


class BlogPost extends Component {
    render() {
        return (
            <Fragment>
                <p className="section-title" >Blog Post</p>
                <div className="post">
                    <div className="img-tumb">
                        <img src="http://placeimg.com/200/150/tech" alt="dummy" />
                    </div>
                    <div className="content">
                        <p className="title">dump title</p>
                        <p className="desc">dump desc</p>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default BlogPost;