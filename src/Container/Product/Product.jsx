import React, { Component, Fragment } from "react";
import CardProduct from '../CardProduct/CardProduct';
import './Product.css';

class Product extends Component {
    state = {
        order: 0
    }

    handleCounterChange = (newOrder) => {
        this.setState({
            order : newOrder
        }
        )
    }

    render() {
        return (
            <Fragment>
                <div className="header">
                    <div className="logo">
                        <img src="" />
                    </div>
                    <div className="troley">
                        <img src="https://image.shutterstock.com/image-vector/trolley-logo-shopping-icon-buy-260nw-1103146715.jpg" />
                        <div className="count">{this.state.order}</div> 
                    </div>
                    
                </div>
                <CardProduct onCounterChange ={(valueOrder) => this.handleCounterChange(valueOrder)} />
            </Fragment>
        )
    }
}

export default Product;